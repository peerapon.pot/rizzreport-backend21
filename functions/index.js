Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
}
exports.generateId = async (fnCount = () => { }, fnDuplicate = () => { }, prefix = '', position = 0, pad = 6) => {

    const date = new Date();
    const month = date.getMonth();
    const year = date.getFullYear();
    let count = 1 + position;
    const num_register = await fnCount(year, month);

    let id = `${prefix}${year.toString().substr(-2)}${Number(month + 1).pad(2)}${Number(num_register + count).pad(pad)}`;

    while (await fnDuplicate(id)) {
        count++;
        const month = date.getMonth();
        const year = date.getFullYear();
        const num_register = await fnCount(year, month);
        id = `${prefix}${year.toString().substr(-2)}${Number(month + 1).pad(2)}${Number(num_register + count + position).pad(pad)}`;
    }


    return id;
}