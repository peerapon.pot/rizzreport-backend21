const sqlConfig = function () {
    var config = {
        user: 'WebProduction',
        password: 'dplusProduction',
        server: '192.168.3.21',
        database: 'Data_TransportApp',
        requestTimeout: 300000,
        pool: {
            idleTimeoutMillis: 300000,
            max: 100
        },

    };
    return config;
};



// const dbConnectData_Rizz = function () {
//     var config = {
//         user: 'webproduction',
//         password: 'dplusProduction',
//         server: '192.168.3.21', // You can use 'localhost\\instance' to connect to named instance
//         database: 'Data_RizzService',
//         multipleStatements: true,
//         requestTimeout: 300000,
//         pool: {
//             idleTimeoutMillis: 300000,
//             max: 100
//         },
//         options: {
//             encrypt: true, // Use this if you're on Windows Azure
//         }
//     }
//     return config

// }
const dbConnectData_Rizz = {
    client: 'mssql',
    connection: {
        user: 'webproduction',
        password: 'dplusProduction',
        host: '192.168.3.21', // You can use 'localhost\\instance' to connect to named instance
        database: 'Data_RizzService',
        pool: {
            idleTimeoutMillis: 300000,
            max: 100
        },
        options: {
            encrypt: true, // Use this if you're on Windows Azure
        }
    }
}
const dbConnectData_temp = {
    client: 'mssql',
    connection: {
        user: 'webproduction',
        password: 'dplusProduction',
        host: '192.168.3.32', // You can use 'localhost\\instance' to connect to named instance
        database: 'Data_Temp',
        pool: {
            idleTimeoutMillis: 300000,
            max: 100
        },
        options: {
            encrypt: true, // Use this if you're on Windows Azure
        }
    }
}




module.exports = {
    condb1: sqlConfig,
    dbConnectData_Rizz: dbConnectData_Rizz,
    dbConnectData_temp: dbConnectData_temp,

};