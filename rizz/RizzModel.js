const knex = require('../connect')
const moment = require('moment')


class rizzModel {

    async getProduct() {
        return knex.raw(`select * from dropoff_claim`)
    }
    async insertProduct(data) {

        return knex.raw(`insert into productRegister 
        (line_id,product_type,register_date,register_id,
        is_completed,dropoff_id,date_end_warranty,date_end_warranty_source,telephone,code,name)VALUES${data}`)
    }
    async insertCustomer(data) {
        // console.log(`insert into customerFollow 
        // (userId,status,date_follow,date_unfollow)
        // VALUES${data}`)

        return knex.raw(`insert into customerFollow 
        (userId,status,date_follow,date_unfollow)
        VALUES${data}`)
    }
    async insertClaim(data) {
        // console.log(`insert into claim 
        // (claim_doc,claim_id,is_completed,status,type_register,userId,claim_date,description,dropoffclaim_id)
        // VALUES${data}`)

        return knex.raw(`insert into claim 
        (claim_doc,claim_id,is_completed,status,type_register,userId,claim_date,description,dropoffclaim_id,product_id,remark)
        VALUES${data}`)
    }

    async deleteProduct() {
        return knex(`productRegister`).del()
    }
    async deleteCustomer() {
        return knex(`customerFollow`).del()
    }
    async deleteClaim() {
        return knex(`claim`).del()
    }
	async getRizzReportByDate(start_date,end_date) {
		console.log(start_date,end_date)
		return knex.raw(`EXEC SP_getRizzReportByDate '${start_date}','${end_date}';`)
		//return knex.raw(`EXEC SP_getRizzReportByDate '06-01-2020','06-30-2020';`)
	}
}
module.exports = new rizzModel()