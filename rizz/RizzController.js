const con = require('../db')
const sql = require('mssql')
const moment = require('moment')
const fetch = require('node-fetch');
let pool;
const rizzModel = require('./RizzModel')
const { success, falss } = require('../response')

const getCustomerFollow = () => new Promise((resolve, reject) => {
   // fetch(`http://18.140.21.41:4500/api/v1/rizz/export/export_customerFollow`, {
	fetch(`https://www.service-frontd.com:4500/api/v1/rizz/export/export_customerFollow`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
        json: true
    }).then(res => {
        setTimeout(() => null, 0);
        return res.json()
    }).then(async json => {
        // Insert_Product((json))
        resolve(json)
    }).catch((err) => console.log(err))
})
const getClaim = () => new Promise((resolve, reject) => {
    fetch(`https://www.service-frontd.com:4500/api/v1/rizz/export/export_Claim`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
        json: true
    }).then(res => {
        setTimeout(() => null, 0);
        return res.json()
    }).then(async json => {
        // Insert_Product((json))
        resolve(json)
    }).catch((err) => console.log(err))
})
const getProductRegister = () => new Promise((resolve, reject) => {
    fetch(`https://www.service-frontd.com:4500/api/v1/rizz/export/export_productRegister`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
        json: true
    }).then(res => {
        setTimeout(() => null, 0);
        return res.json()
    }).then(async json => {
        // Insert_Product((json))
        resolve(json)
    }).catch((err) => console.log(err))
})

const insertProduct = async (arr) => {
    let el = arr.result
//console.log(el)
    try {
        el.map(async el => {

            let register_date = el.register_date === null ? null : `'${el.register_date}'`
            let date_end_warranty = el.date_end_warranty === null ? null : `'${el.date_end_warranty}'`
            const result = `('${el.line_id}','${el.product_type}',${register_date},'${el.register_id}','${el.is_completed}',${el.dropoff_id},${date_end_warranty},'${el.date_end_warranty_source}','${el.telephone}','${el.code}','${el.name}')`
          //  console.log(result)
              await rizzModel.insertProduct(result)

        })

    } catch (error) {
        console.log(error)
    }


}

const insertCustomer = async (arr) => {
    let el = arr.result

    try {
        el.map(async el => {
            // console.log(el.line_id)
            if (el.userId !== undefined) {
                let date_follow = el.date_follow === null ? null : `'${el.date_follow}'`
                let date_unfollow = el.date_unfollow === null ? null : `'${el.date_unfollow}'`
                const result = `('${el.userId}','${el.status}',${date_follow},${date_unfollow})`
                //console.log(result)
                await rizzModel.insertCustomer(result)
            }


        })

    } catch (error) {
        console.log(error)
    }


}
const insertClaim = async (arr) => {
    let el = arr.result

    try {
        el.map(async el => {
            // console.log(el.line_id)

            let claim_date = el.claim_date === null ? null : `'${el.claim_date}'`
        

            const result = `('${el.claim_doc}','${el.claim_id}','${el.is_completed}','${el.status}','${el.type_register}','${el.userId}',${claim_date},'${el.description}','${el.dropoffclaim_id}','${el.product_id}','${el.remark}')`

            await rizzModel.insertClaim(result)



        })

    } catch (error) {
        console.log(error)
    }


}


exports.info_product = async () => {

    try {

        const result = await getProductRegister()
        console.log(result)
        await rizzModel.deleteProduct()
        await insertProduct(result)
        return { res: 'success', status: 200 }
    } catch (error) {
        console.log(error)

        return { res: 'false', status: 500 }


    }
}
exports.info_customer = async () => {
    try {
        const result = await getCustomerFollow()
        await rizzModel.deleteCustomer()
        await insertCustomer(result)
        return { res: 'success', status: 200 }

    } catch (error) {
        console.log(error)

        return { res: 'false', status: 500 }

    }
}
exports.info_claim = async () => {
    try {
        const result = await getClaim()
        await rizzModel.deleteClaim()
        await insertClaim(result)
        return { res: 'success', status: 200 }

    } catch (error) {
        console.log(error)

        return { res: 'false', status: 500 }

    }
}
exports.rizz_Report = async (start_date,end_date) => {
	console.log(start_date,end_date)
	try {
		const result = await rizzModel.getRizzReportByDate(start_date,end_date)
		return {res:result,status:200}
	} catch(error) {
		console.log(error)
		return {res:'false',status:500}
	}
	
}

