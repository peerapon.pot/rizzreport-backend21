const router = require('express').Router();

const rizzController = require('./RizzController')




router.get('/rizz/api/export_productRegister', async (req, res) => {

    let reponse = await rizzController.info_product()
    res.status(reponse.status).json({ result: reponse.res, status: reponse.status })
})
router.get('/rizz/api/export_customerFollow', async (req, res) => {

    let reponse = await rizzController.info_customer()
    res.status(reponse.status).json({ result: reponse.res, status: reponse.status })
})
router.get('/rizz/api/export_Claim', async (req, res) => {
    console.log('sss')
    let reponse = await rizzController.info_claim()
    res.status(reponse.status).json({ result: reponse.res, status: reponse.status })
})
router.get('/rizz/api/getRizzReportByDate',async (req,res) =>{
	let response = await rizzController.rizz_Report(req.query.start_date,req.query.end_date)
	  res.status(response.status).json({ result: response.res, status: response.status })
})


module.exports = router;