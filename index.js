const express = require('express');
const app = express();
const moment = require('moment');
const bodyParser = require('body-parser');
const schedule = require('node-schedule');
const cors = require('cors');
const rizz = require('./rizz/RizzController')


const whitelist = [
    `http://localhost:3000`,
    `http://localhost:5500`,
   
    `http://192.168.1.122:3000`,
    `https://www.sheetme.com`,
	`https://www.dpluscrm.com`,
	`https://dpluscrm.com`,
    null,
    undefined
]

const corsOption = {
    origin: (origin, cb) => {
        //debug('origin %o', origin);
        if (whitelist.indexOf(origin) !== -1) {
            cb(null, true)
        } else {
            cb(new Error('Not allows by Cors'))
        }
    },
    optionsSuccessStatus: 200,
    preflightContinue: true,
    credentials: true
}

app.use(cors(corsOption));
app.use(bodyParser.urlencoded({ extended: true, limit: 1024 * 1024 * 20, type: 'application/x-www-form-urlencoding' }))
app.use(bodyParser.json({ limit: 1024 * 1024 * 20, type: 'application/json' }))

require('./connect'); // use knex

/*###################### ROUTER  ######################*/

const rizz_router = require('./rizz/RizzRouter')

app.use("/", rizz_router)
rizz.info_product()
/*###################### SCHEDULE TRACKING DHL ######################*/
schedule.scheduleJob("0 0 * * *", async () => {
    console.log("Schedule :::", moment().format())
    rizz.info_product()
    rizz.info_claim()
    rizz.info_customer()
})

app.listen(9090, () => {
    console.log("Server is ready, PORT use " + 9090)
})