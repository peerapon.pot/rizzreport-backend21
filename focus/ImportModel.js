
const knex = require('../connect_temp')
const moment = require('moment');

class ImportModel {

    getProductTemp() {
        return knex('DPLT_PDEX_BomJournal')
            .where({ Id: 41251 })
            .first()
    }
    countJournalNumberOnMonth() {

        const month_start = moment(new Date()).format('YYYY-MM-DD');
        return knex('DPLT_PDEX_BomJournal')
            .where(`CreatedDate`, `>`, `${month_start}`)
            .count('Id as count')
            .then(r => r[0].count)
    }
    checkDuplicateJournalId(JournalId = '') {
        return knex('DPLT_PDEX_BomJournal')
            .where({ JournalId })
            .first()
            .then(r => !!r)
    }
    insertToDataTemp(obj) {

        return knex('DPLT_PDEX_BomJournal')
            .returning(['Id'])
            .insert(obj)

    }

}
module.exports = new ImportModel()