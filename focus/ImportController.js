// import { success, failed } from '../../config/response'
// import { err, debug } from '../../config/debug';
const ImportModel = require('./ImportModel');

const moment = require('moment');
const { generateId } = require('../functions')
const fetch = require('node-fetch');

exports.getProduct = async () => {
    try {
        console.log('getproduct')
        let result = await ImportModel.getProductTemp()
        console.log(result)
        return { res: result, status: 200 }

    } catch (error) {
        console.log(error)

        return { res: 'false', status: 500 }

    }
}
const getAxDocument = (doc) => new Promise((resolve, reject) => {
    console.log(doc)
    fetch(`http://192.168.3.32:88/GeneralJournal.svc/create`, {

        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ company: 'DPL', journalId: `PE:${doc}` }),

    }).then(res => {
        setTimeout(() => null, 0);


        return res.json()
    }).then(async json => {
        resolve(json)
    }).catch((err) => console.log(err))
})
const updateWarranty = (obj) => new Promise((resolve, reject) => {

    fetch(`http://192.168.22.133:5500/api/v1/focus/warranty/updateWarrantyFromAx`, {

        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ no_warranty: obj.no_warranty, import_status: obj.import_status, import_doc: obj.import_doc }),

    }).then(res => {
        setTimeout(() => null, 0);


        return res.json()
    }).then(async json => {
        resolve(json)
    }).catch((err) => console.log(err))
})





exports.insertProducet = async (req) => {

    try {
        let data = req;
        let journalNameId = [];
        console.log(req)
        let CreatedDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')

        journalNameId = await Promise.all(data.map(async (el, i) => {
            let obj = {
                JournalId: await generateId(
                    async () => await ImportModel.countJournalNumberOnMonth(),
                    async (JournalId) => await ImportModel.checkDuplicateJournalId(JournalId),
                    'CRM', i
                ),//function generate 
                JournalNameId: 'TW1D',
                JournalType: 2,
                JournalDescription: 'TransferToCRM',
                JournalOption: 0,
                BOMLine: 0,
                ItemId: el.item_code,
                Quantity: -1,
                FromSite: 'DPLUS',
                FromWarehouse: 'W1D',
                ToSite: 'DPLUS',
                ToWarehouse: 'ZMK',
                Tolocation: 'CRM',
                RefDocument: el.no_warranty,
                CreatedDate,
                CreatedByUser: 'dplusserve_focus',
                WebStatus: 0,
                Company: 'DPL'
            }
            await ImportModel.insertToDataTemp(obj)
            let axDoc = await getAxDocument(obj.JournalId)
            let split_data = axDoc.replace(/ /g, "").split(":")
            console.log(split_data)
            if (split_data[0] === 'Completed;AXExecutionId') {
                console.log(split_data[2].replace(/;/g, ""))
                let import_doc = split_data[2].replace(/;/g, "")
                let obj = {
                    import_status: '1',
                    import_doc,
                    no_warranty: el.no_warranty,

                }
                try {
                    await updateWarranty(obj)
                } catch (error) {
                    console.log(error)
                }


            }
            else {
                let obj = {
                    import_status: '2',
                    import_doc: '',
                    no_warranty: el.no_warranty,
                }
                try {
                    await updateWarranty(obj)
                } catch (error) {
                    console.log(error)
                }

            }


            return obj.JournalId

        }))
        return { res: 'success', status: 200 }


    } catch (error) {
        console.log(error)
        return { res: 'false', status: 500 }
    }
}
